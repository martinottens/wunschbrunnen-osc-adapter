import express, { Request, Response } from 'express';

const PORT: number = 8081;

type messageEntry = {
    id: number,
    created: number,
    comment_text: string
}

let app: express.Application = express();
let comments: messageEntry[] = [];
let currentId: number = 1;

app.get('/', (req: Request, res: Response) => {
    pageContent(res);
});

app.post('/', (req: Request, res: Response) => {

    let newEntry: messageEntry = {
        id: currentId,
        created: Date.now(),
        comment_text: 'Das ist Wunsch Nummer ' + currentId
    };
    comments.push(newEntry);
    console.log(`POST /: Lege Eintrag ${currentId} an`);
    currentId++;

    pageContent(res);
});

app.get('/comments', (req: Request, res: Response) => {
    let since: any = Number(req.query.since) || 0;
    since *= 1000;

    res.type('json');
    res.charset = 'utf-8';

    let filteredList: any = comments
            .filter((m: messageEntry) => m.created >= since)
            .map((m: messageEntry) => {
                return {
                    id: m.id,
                    created: getAsISODate(m.created),
                    comment_text: m.comment_text
                };
            });

    console.log(`GET /comments: Sende ${filteredList.length} Einträge`);

    res.send(JSON.stringify({
        comments: filteredList
    }));
});

app.listen(PORT, () => console.log(`
TEST SERVER GESTARTET, GEHE ZU http://localhost:${PORT}
-------------------------------------------------------
Denke daran im Adapter den Test-Modus zu aktivieren und
diesen dannach neuzustarten.
Verlasse den Test-Server mit STRG + C.
`));


function pageContent(res: Response): void {
    res.type('html');
    res.charset = 'utf-8';
    res.send(`
        <html><title>Wunschbrunnen Test</title><body>
        <h1>Aktuelle Anzahl der Einträge: ${comments.length}</h1>
        <form action="/" method="post">
        <input type="submit" value="Weiteren Eintrag hinzufügen">
        </form>
        </body></html>
    `);
} 

function getAsISODate(unix: number): string {
    return new Date(unix).toISOString().replace(/T/, ' ').replace(/\..+/, '');
}