@echo off & setlocal

call npm install
call npm run build

:RERUN
call npm run run
GOTO RERUN

pause