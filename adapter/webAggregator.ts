import axios, { AxiosResponse } from "axios";
import Joi from "joi";

import { log, logError } from "./index";

const responseSchema: Joi.Schema = Joi.object({
    comments: Joi.array().items(
        Joi.any()
        /*Joi.object({
            id: Joi.number(),
            created: Joi.string(),
            comment_text: Joi.string(),
        })*/
    ).required()
});

export default class WebAggregator {

    readonly url: string;
    readonly triggerCount: number;
    lastTriggeredId: number = 0;


    constructor(url: string, triggerCount: number) {
        this.url = url;
        this.triggerCount = triggerCount;
    }

    public async init(): Promise<void> {
       this.lastTriggeredId = await this.getLastId();
       log('Web-Agg', `Erste Anfrage erfolgreich, es gibt ${this.lastTriggeredId} Wünsche.`);
    } 

    public async check(): Promise<boolean> {
        let result: number = 0;
        try {
            result = await this.getLastId();
        } catch (error) {
            logError('Web-Agg', `Wiederkehrender Check: Fehler beim Webserver-Zugriff: ${error}, probiere später nochmal.`);
            return false;
        }

        let sinceLastTrigger: number = result - this.lastTriggeredId;
        log('Web-Agg', `Wiederkehrender Check: Es gibt ${sinceLastTrigger} neue Wünsche seit dem letzten Trigger.`);

        if (sinceLastTrigger >= this.triggerCount) {
            log('Web-Agg', `${sinceLastTrigger} Wünsche seit letzter Auslösung.`);
            this.lastTriggeredId = result;
            return true;
        } else {
            return false;
        }
    }

    private async getLastId(): Promise<number> {
        let res: AxiosResponse = await axios.get(this.url);
        let { value, error } = responseSchema.validate(res.data);

        if (error) {
            logError('Web-Agg', `Fehler in Antwort des Webservers: ${error}`);
            throw Error("Invalid server response!");
        }

        return Math.max(...value.comments.map((comment: any) => comment.id))
    }
}
