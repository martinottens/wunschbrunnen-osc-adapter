import dgram from 'dgram';
import oscJS from 'osc-js';
import yaml from 'yaml';
import fs from 'fs';
import chalk from 'chalk';
import Joi from 'joi';
import axios, { AxiosResponse } from "axios";

import WebAggregator from './webAggregator';

type Settings = {
    osc_server: string,
    osc_port: number,
    osc_commands: string[],
    osc_paket_delay: number,
    server_url: string,
    trigger_count: number,
    check_seconds: number,
    block_seconds: number,
    ping_enabled: boolean,
    ping_interval: number,
    ping_host: string,
    ping_key: string
}

const SETTINGS: Settings = validateConfig('./config.yml');
const SOCKET: dgram.Socket = dgram.createSocket('udp4');
const WEB_AGGR: WebAggregator = new WebAggregator(SETTINGS.server_url, SETTINGS.trigger_count);

let oscStartRealease: number = 0;

main();

async function main() {
    await WEB_AGGR.init();
    startTimer();

    if (SETTINGS.ping_enabled) {
        pinger();
    }
}

async function pinger(): Promise<void> {
    log('Ping', `Pinger aufgerufen.`);

    try {
        await axios.get(SETTINGS.ping_host + "/" + SETTINGS.ping_key);
    } catch (error) {
        logError('Ping', `Fehler beim Pingen: ${error}, probiere später nochmal.`);
    }

    setTimeout(() => pinger(), SETTINGS.ping_interval* 1000);
}

async function startTimer(): Promise<void> {
    log('Main', `Timer aufgerufen.`);
    timerJob();
    setTimeout(() => startTimer(), SETTINGS.check_seconds * 1000);
}

async function timerJob(): Promise<void> {
    if (oscStartRealease >= Date.now()) {
        log('Main', `Timer ohne Check abgebrochen: Start-Cooldown läuft noch.`);
        return;
    }

    if (await WEB_AGGR.check()) {
        log('Main', `Trigger aktiviert. Sende OSC-Befehle.`);
        await sendOSCMessages(SETTINGS.osc_commands);
        oscStartRealease = Date.now() + SETTINGS.block_seconds * 1000;
    }
}

async function sendOSCMessages(messages: string[]): Promise<void> {
    for (let message of messages) {
        let oscMessage = new oscJS.Message(message).pack();
        SOCKET.send(Buffer.from(oscMessage), 0, oscMessage.length, SETTINGS.osc_port, SETTINGS.osc_server);
        await new Promise(resolve => setTimeout(resolve, SETTINGS.osc_paket_delay));
    }
}

function validateConfig(file: string): Settings {
    let unchecked: any = yaml.parse(fs.readFileSync(file, 'utf-8'));
    let schema: Joi.Schema = Joi.object({
        osc_server: Joi.string().required(),
        osc_port: Joi.number().required(),
        osc_commands: Joi.array().items(
            Joi.string()
        ).required(),
        osc_paket_delay: Joi.number().required(),
        server_url: Joi.string().required(),
        trigger_count: Joi.number().required(),
        check_seconds: Joi.number().required(),
        block_seconds: Joi.number().required(),
        ping_enabled: Joi.boolean().required(),
        ping_interval: Joi.number().required(),
        ping_host: Joi.string().required(),
        ping_key: Joi.string().required()
    });

    const { value, error } = schema.validate(unchecked);

    if (error) {
        logError('Main', `Fehler in der Config: ${error}`);
        process.exit(1);
    }

    return value as Settings;
}
export function log(prefix: string, message: string): void {
    console.log(`[INFO]  ` + new Date().toLocaleString('de-DE') + ` [${prefix}]: ${message}`);
}

export function logError(prefix: string, message: string): void {
    console.log(chalk.red(`[ERROR] ` + new Date().toLocaleString('de-DE') + ` [${prefix}]: ${message}`));
}
