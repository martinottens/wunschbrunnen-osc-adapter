# Wunschbrunnen Adapter
Eine Sammlung von Software für die Anbindung von Websites an eine Audiosteuerung mittels OSC. In diesem Fall speziell für eine Kunstinstallation mit einem bestimmten Website-Backend oder mit Test-Server.

## Verwendung der Software
### Installationen
- Zur Verwendung muss eine halbwegs aktuelle Installation von NodeJS und npm vorhanden sein: https://nodejs.org/en/ (LTS-Version)
- Ein OSC-Server, bspw. Reaper, muss so konfiguriert werden, dass es eingehende OSC-UDP-Verbindungen akzeptiert

### Konfiguration
In der Datei `config.yml` finden sich einige Einstellungsmöglichkeiten inkl. Kommentare.

Besonders wichtig: `server_url` in der Config muss angepasst werden. Die Original Server-URL bitte jedoch nur bis zum ersten '?' eintragen, der Rest muss weggelassen werden.

### Programm starten
Starte die `run.bat`. Diese hält die Software am laufen - auch wenn es zu einem Fehler kommen sollte. In der Eingabeaufforderung, welche sich öffnet, können Details zum Programmablauf abgelesen werden.

## Test-Server
In dieser Software-Sammlung findet sich ein lokaler Test-Server zum einfachen Testen der Installation. Gestartet wird dieser Server mit der Datei `run-test-server.bat`. Öffne nun http://localhost:8081 im Browser auf dem Computer, auf dem der Test-Server läuft, für ein einfaches Webinterface. Damit auch die OSC-Adapter-Software auf den Testserver hört, muss in der `config.yml` die Einstellung `server_url` angepasst werden und das Programm anschließend neugestartet werden, falls er vorher bereits lief. 